import bridge from '@vkontakte/vk-bridge';
import React from "react";
import {IMG_PATH} from "../components/App/constants";

export function share() {
  let text = "Я тренируюсь, я молодец";

  bridge.send('VKWebAppShowWallPostBox', {
    "message": text,
  });
}

export function getImg(id) {
  return IMG_PATH + id + '.jpg';
}

export function inNames(search, name, altName, engName) {
  search = search.toLowerCase();
  return name.toLowerCase().indexOf(search) !== -1 || altName.toLowerCase().indexOf(search) !== -1 || engName.toLowerCase().indexOf(search) !== -1;
}