import React from 'react';
import PropTypes from "prop-types";
import LazyLoad from 'react-lazy-load';

const LazyImg = ({ src, height, width, style }) => (<LazyLoad height={height} width={width}>
    <img style={style} src={src} />
  </LazyLoad>);

LazyImg.propTypes = {
  src: PropTypes.string.isRequired,
  height: PropTypes.number.isRequired,
  width: PropTypes.number.isRequired,
  style: PropTypes.object,
};

export default LazyImg;